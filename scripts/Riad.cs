using System;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

namespace Scripting
{
    public class Riad 
    {
        // [DllImport ("__Internal", EntryPoint="riad_mono_print")]
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void print(string message);
    }
}