
#include "localfile.h"

namespace Base {

LocalFile::LocalFile(const std::string& path)
{
    m_info.path = path;
}

LocalFile::~LocalFile() 
{
}

bool LocalFile::isOpen() const 
{
    return m_stream && m_stream.is_open();
}

Interface::File::Info LocalFile::info() const 
{
    return m_info;
}

bool LocalFile::open(uint8_t access)
{
    if(m_stream.is_open())
    {
        return false;
    }
    m_info.exists = false;
    m_info.size = 0;
    m_info.flags = access;

    std::ios_base::openmode mode = std::ios::binary;
    if(m_info.flags & Access::READ)
    {
        mode |= std::ios::in;
    }
    if(m_info.flags & Access::WRITE)
    {
        mode |= std::ios::out;
    }
    if(m_info.flags & Access::APPEND)
    {
        mode |= std::ios::app;
    }

    m_stream = std::fstream(m_info.path, mode);

    if(!m_stream)
    {
        return false;
    }

    m_stream.seekg(0, std::ios::end);
    m_info.size = m_stream.tellg();
    m_stream.seekg(0, std::ios::beg);
    
    return true;
}

void LocalFile::reset()
{
	m_stream.close();
}

void LocalFile::close()
{
	m_stream.close();
}

void LocalFile::flush()
{
	m_stream.flush();
}

void LocalFile::seek(size_t position)
{
    m_stream.seekg(position, std::ios::beg);
}

size_t LocalFile::get()
{
    return m_stream.tellg();
}

size_t LocalFile::read(size_t size, uint8_t *buffer)
{
    m_stream.read((char*)buffer, size);
    
    if(!m_stream)
    {
        return m_stream.gcount();
    }
    return size;
}

size_t LocalFile::write(size_t size, const uint8_t *buffer)
{
    m_stream.write((char*)buffer, size);
    
    if(!m_stream)
    {
        return m_stream.gcount();
    }
    return size;
}

size_t LocalFile::read(size_t offset, size_t size, uint8_t *buffer)
{
    seek(offset);
    return read(size,buffer);
}

size_t LocalFile::write(size_t offset, size_t size, const uint8_t *buffer)
{
    seek(offset);
    return write(size,buffer);
}

} // ns Base

