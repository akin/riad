
#include <base/taskmanager.h>

namespace Base {

TaskManager::TaskManager()
: m_alive{true}
{
}

TaskManager::~TaskManager()
{
}

void TaskManager::run()
{
    m_running++;
    while(m_alive.load())
    {
        auto task = m_queue.pop(true);
        if(task) 
        {
            task->run();
        }
    }
    m_running--;
}

void TaskManager::close()
{
    m_alive = false;
    clear();
    m_queue.invalidate();

    // Wait for m_running to hit 0
    std::unique_lock<std::mutex> lock{m_mutex};
    m_condition.wait(lock, [this]()
    {
        return m_running == 0;
    });
}

void TaskManager::clear()
{
    m_queue.clear();
}

void TaskManager::add(Interface::Task::Shared work)
{
    m_queue.push(work);
}

} // ns Base
