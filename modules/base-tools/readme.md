
# BASE-TOOLS
 *  platform level implementations
 *  not limited to platform dependent tools.
 *  helpful tools & concepts (logging, filesystem etc.)
 *  light dependencies, platform etc. tools to get things done.
 *  CMake
 *  C++11 (not limited, even 14/17/19 is accepted)
 *  static linking

## TODO
 * Taskmanager need more robust shutdown, the current implementation is totally unaware of everything.