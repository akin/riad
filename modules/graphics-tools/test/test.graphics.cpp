
#ifndef BASE_TOOLS_IMPORT_TEST_H_
#define BASE_TOOLS_IMPORT_TEST_H_

#include <iostream>
#include "catch.hpp"

#include <base/local.h>
#include <graphics/texture2d.h>
#include <graphics/sampler2d.h>
#include <graphics/load.h>
#include <graphics/save.h>

#ifndef TEST_SOURCE_FOLDER
# error Missing TEST_SOURCE_FOLDER define! cannot do testing without source files.
#endif

TEST_CASE( "Simple base-tools tests graphics.", "[]" ) {
    const std::string path(TEST_SOURCE_FOLDER);

    // Load image

    SECTION( "Load original image" ) {
        auto originFile = Base::Local::open(path + "/balls.png");
        Graphics::Texture2D<Graphics::RGB8> origin;

        REQUIRE(originFile->open(Base::READ));

        REQUIRE(Graphics::load(originFile, origin));
        
        REQUIRE(origin.getWidth() == 619 );
        REQUIRE(origin.getHeight() == 600 );
    }

    // Save image
    // Load image
    // Compare pixels
    size_t width = 256;
    size_t height = 256;
    Graphics::Texture2D<Graphics::RGB8> testSource(width, height);
    
    // 256x256 RGB
    for(size_t y = 0 ; y < width ; ++y)
    {
        for(size_t x = 0 ; x < height ; ++x)
        {
            auto& pixel = testSource.at(x, y);
            size_t nx = width - x;
            size_t ny = height - y;
            
            pixel = Graphics::RGB8{
                (uint8_t)(0xFF - (uint8_t)std::min(std::sqrtf(x*x + y*y), 255.0f)),
                (uint8_t)(0xFF - (uint8_t)std::min(std::sqrtf(nx*nx + y*y), 255.0f)),
                (uint8_t)(0xFF - (uint8_t)std::min(std::sqrtf(nx*nx + ny*ny), 255.0f))
            };
        }
    }

    std::string testPath = "test.png";
    auto testFile = Base::Local::open(testPath);
    REQUIRE(testFile->open(Base::WRITE));
    REQUIRE(Graphics::save(testFile, testSource));
    testFile->close();
    
    SECTION( "sRGB Conversion" ) {
        // sRGB Conversion
        Graphics::Texture2D<Graphics::SRGB8> test(testSource);
        
        std::string testPath2 = "test_srgb.png";
        auto testFile2 = Base::Local::open(testPath2);
        REQUIRE(testFile2->open(Base::WRITE));
        REQUIRE(Graphics::save(testFile2, test));
        testFile2->close();
    }

    SECTION( "Blur filtering" ) {
        // sRGB Conversion
        using PixelFormat = Graphics::RGB8;
        Graphics::Texture2D<PixelFormat> test(testSource);
        Graphics::Sampler2D<PixelFormat> sampler(testSource);
        
        // blur
        test.filter([&sampler](size_t x, size_t y, PixelFormat& pixel){
            float fx = x;
            float fy = y;

            int r = 0;
            int g = 0;
            int b = 0;

            for(float y = -1 ; y <= 1.0f ; y += 1.0f)
            {
                for(float x = -1 ; x <= 1.0f ; x += 1.0f)
                {
                    const auto c = sampler.at(fx + x,fy + y);

                    r += color::get::red(c);
                    g += color::get::green(c);
                    b += color::get::blue(c);
                }
            }

            r /= 9;
            g /= 9;
            b /= 9;

            pixel = PixelFormat{
                (uint8_t)(r),
                (uint8_t)(g),
                (uint8_t)(b)
            };
        });
        
        // invert
        test.filter([](PixelFormat& pixel){
            pixel = PixelFormat{
                (uint8_t)(0xFF - color::get::red(pixel)),
                (uint8_t)(0xFF - color::get::green(pixel)),
                (uint8_t)(0xFF - color::get::blue(pixel))
            };
        });
        
        std::string testPath2 = "test_blur_invert.png";
        auto testFile2 = Base::Local::open(testPath2);
        REQUIRE(testFile2->open(Base::WRITE));
        REQUIRE(Graphics::save(testFile2, test));
        testFile2->close();
    }

    SECTION( "Sampler offset testing" ) {
        // sRGB Conversion
        using PixelFormat = Graphics::RGB8;
        Graphics::Texture2D<PixelFormat> test(testSource);
        Graphics::Sampler2D<PixelFormat> sampler(testSource);
        
        // blur
        test.filter([&sampler](size_t x, size_t y, PixelFormat& pixel){
            float fx = x + 128;
            float fy = y + 128;

            const auto color = sampler.at(fx,fy);

            pixel = PixelFormat{
                color::get::red(color),
                color::get::green(color),
                color::get::blue(color)
            };
        });
        
        std::string testPath2 = "test_sampler_offset_128.png";
        auto testFile2 = Base::Local::open(testPath2);
        REQUIRE(testFile2->open(Base::WRITE));
        REQUIRE(Graphics::save(testFile2, test));
        testFile2->close();
    }
    
    SECTION( "Sampler 10x tile testing" ) {
        // sRGB Conversion
        using PixelFormat = Graphics::RGB8;
        Graphics::Texture2D<PixelFormat> test(testSource);
        Graphics::Sampler2D<PixelFormat> sampler(testSource);
        
        // blur
        test.filter([&sampler](size_t x, size_t y, PixelFormat& pixel){
            float fx = x * 10;
            float fy = y * 10;
            
            const auto color = sampler.at(fx,fy);
            
            pixel = PixelFormat{
                color::get::red(color),
                color::get::green(color),
                color::get::blue(color)
            };
        });
        
        std::string testPath2 = "test_sampler_10x.png";
        auto testFile2 = Base::Local::open(testPath2);
        REQUIRE(testFile2->open(Base::WRITE));
        REQUIRE(Graphics::save(testFile2, test));
        testFile2->close();
    }
    
    SECTION( "Sampler half tile testing" ) {
        // sRGB Conversion
        using PixelFormat = Graphics::RGB8;
        Graphics::Texture2D<PixelFormat> test(testSource);
        Graphics::Sampler2D<PixelFormat> sampler(testSource);
        
        // blur
        test.filter([&sampler](size_t x, size_t y, PixelFormat& pixel){
            float fx = 32 + x * 0.5;
            float fy = 32 + y * 0.5;
            
            const auto color = sampler.at(fx,fy);
            
            pixel = PixelFormat{
                color::get::red(color),
                color::get::green(color),
                color::get::blue(color)
            };
        });
        
        std::string testPath2 = "test_sampler_half.png";
        auto testFile2 = Base::Local::open(testPath2);
        REQUIRE(testFile2->open(Base::WRITE));
        REQUIRE(Graphics::save(testFile2, test));
        testFile2->close();
    }
    
    SECTION( "Copy constructor Conversion" ) {
        // sRGB Conversion
        Graphics::Texture2D<Graphics::RGB8> test(testSource);
        
        std::string testPath2 = "test_copy.png";
        auto testFile2 = Base::Local::open(testPath2);
        REQUIRE(testFile2->open(Base::WRITE));
        REQUIRE(Graphics::save(testFile2, test));
        testFile2->close();
    }
    
    SECTION( "HDR test Conversion" ) {
        // sRGB Conversion
        Graphics::Texture2D<Graphics::RGBf> hdr(width, height);
        float max = 256.0f;
        
        for(size_t y = 0 ; y < width ; ++y)
        {
            for(size_t x = 0 ; x < height ; ++x)
            {
                auto& pixel = hdr.at(x, y);
                size_t nx = width - x;
                size_t ny = height - y;
                
                pixel = Graphics::RGBf{
                    std::sqrtf(x*x + y*y) / max,
                    std::sqrtf(nx*nx + y*y) / max,
                    std::sqrtf(nx*nx + ny*ny) / max
                };
            }
        }
        
        hdr.filter([](Graphics::RGBf& pixel){
            pixel = Graphics::RGBf{
                std::min(color::get::red(pixel), 1.0f),
                std::min(color::get::blue(pixel), 1.0f),
                std::min(color::get::green(pixel), 1.0f)
            };
        });
        
        {
            Graphics::Texture2D<Graphics::RGB8> ser(hdr);
            std::string path = "test_hdr_to_ldr.png";
            auto file = Base::Local::open(path);
            REQUIRE(file->open(Base::WRITE));
            REQUIRE(Graphics::save(file, ser));
            file->close();
        }
    }

    SECTION( "Load test.png image" ) {
        auto testFile2 = Base::Local::open(testPath);
        REQUIRE(testFile2->open(Base::READ));
        Graphics::Texture2D<Graphics::RGB8> loaded;
        REQUIRE(Graphics::load(testFile2, loaded));
        
        REQUIRE(loaded.getWidth() == width);
        REQUIRE(loaded.getHeight() == height);
        
        for(size_t y = 0 ; y < width ; ++y)
        {
            for(size_t x = 0 ; x < height ; ++x)
            {
                auto source = testSource.at(x,y);
                auto target = loaded.at(x,y);
                
                bool pass = true;
                
                if(color::get::red(source) != color::get::red(target))
                {
                    pass = false;
                    std::cout << "r " << x << "x" << y << " " << (int)color::get::red(target) << "!=" << (int)color::get::red(source) << std::endl;
                }
                if(color::get::green(source) != color::get::green(target))
                {
                    pass = false;
                    std::cout << "g " << x << "x" << y << " " << (int)color::get::green(target) << "!=" << (int)color::get::green(source) << std::endl;
                }
                if(color::get::blue(source) != color::get::blue(target))
                {
                    pass = false;
                    std::cout << "b " << x << "x" << y << " " << (int)color::get::blue(target) << "!=" << (int)color::get::blue(source) << std::endl;
                }
                REQUIRE( pass );
            }
        }
    }
}

#endif // BASE_TOOLS_IMPORT_TEST_H_
