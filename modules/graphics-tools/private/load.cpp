
#include <graphics/load.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <fstream>

#include <string>
#include <graphics/color.h>
#include <graphics/texture2d.h>

namespace Graphics {

bool load(Interface::File::Shared file, Texture2D<RGB8>& texture)
{
    if(!file->isOpen())
    {
        return false;
    }
    auto info = file->info();

    std::vector<uint8_t> filebuffer;
    filebuffer.resize(info.size);

    size_t count = file->read(info.size, filebuffer.data());
    if(count != info.size)
    {
        return false;
    }

    int x,y,channels;
    stbi_uc *buffer = stbi_load_from_memory(filebuffer.data(), filebuffer.size(), &x, &y, &channels, 3);
    if(buffer == nullptr)
    {
        return false;
    }
    texture.resize(x,y);
    memcpy(texture.data(), buffer, texture.sizeBytes());
    stbi_image_free(buffer);
    return true;
}

bool load(Interface::File::Shared file, Texture2D<RGBA8>& texture)
{
    if(!file->isOpen())
    {
        return false;
    }
    auto info = file->info();

    std::vector<uint8_t> filebuffer;
    filebuffer.resize(info.size);

    size_t count = file->read(info.size, filebuffer.data());
    if(count != info.size)
    {
        return false;
    }

    int x,y,channels;
    stbi_uc *buffer = stbi_load_from_memory(filebuffer.data(), filebuffer.size(), &x, &y, &channels, 4);
    if(buffer == nullptr)
    {
        return false;
    }
    texture.resize(x,y);
    memcpy(texture.data(), buffer, texture.sizeBytes());
    stbi_image_free(buffer);
    return true;
}

} // ns Graphics

