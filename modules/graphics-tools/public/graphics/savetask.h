
#ifndef BASE_TOOLS_GRAPHICS_SAVETASK_H_
#define BASE_TOOLS_GRAPHICS_SAVETASK_H_

#include <interface/task.h>
#include <graphics/save.h>
#include <base/local.h>
#include <base/log.h>

namespace Graphics {

template <class ColorFormat>
class SaveTask : public Interface::Task {
    BASE_ClassDef(SaveTask<ColorFormat>);
private:
    Texture2D<ColorFormat> m_texture;
    std::string m_file;
public:
    virtual ~SaveTask()
    {
    }

    // Texture
    // Where to save
    // What to do on success?
    // What to do on failure?
    void setFilename(std::string file)
    {
        m_file = file;
    }

    Texture2D<ColorFormat>& getTexture()
    {
        return m_texture;
    }

    void run() override
    {
        auto file = Base::Local::open(m_file);
        if(!file->open(Base::Access::WRITE))
        {
            // FAIL
        }

        if(!save(file, m_texture))
        {
            // FAIL
        }

        // Success
    }
};

} // ns Graphics

#endif // BASE_TOOLS_GRAPHICS_SAVETASK_H_
