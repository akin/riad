
#ifndef BASE_TOOLS_GRAPHICS_COLOR_H_
#define BASE_TOOLS_GRAPHICS_COLOR_H_

#include <color/color.hpp>

namespace Graphics {

using RGB8 = color::rgb<uint8_t>;
using SRGB8 = color::xyz<uint8_t>;
using RGBA8 = color::rgba<uint8_t>;

using RGBf = color::rgb<float>;
using SRGBf = color::xyz<float>;
using RGBAf = color::rgba<float>;

} // ns Base

#endif // BASE_TOOLS_GRAPHICS_COLOR_H_
