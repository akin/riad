cmake_minimum_required(VERSION 3.7)
project(testbed C CXX)
add_library(${PROJECT_NAME} STATIC)

set(CMAKE_MODULE_PATH 
	${CMAKE_MODULE_PATH} 
	"${CMAKE_CURRENT_SOURCE_DIR}/cmake"
)

file(GLOB CURRENT_SOURCES 
	${CMAKE_CURRENT_LIST_DIR}/private/*.cpp 
	${CMAKE_CURRENT_LIST_DIR}/private/*.c
	${CMAKE_CURRENT_LIST_DIR}/private/*.inl
	${CMAKE_CURRENT_LIST_DIR}/private/*.h
	${CMAKE_CURRENT_LIST_DIR}/public/testbed/*.inl
	${CMAKE_CURRENT_LIST_DIR}/public/testbed/*.h
)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_SOURCES}
	)

target_include_directories(
	${PROJECT_NAME} 
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/private"
	PUBLIC 
		"${CMAKE_CURRENT_LIST_DIR}/public"
)


target_link_libraries(
    ${PROJECT_NAME} 
	PUBLIC
		glm
		fmt
		sqlite3
)

add_subdirectory("test")