#include <testbed/cachetest.h>
#include <glm/gtx/quaternion.hpp>

namespace TestBed
{
	void resize(TransformData& data, size_t size)
	{
		data.location.resize(size);
		data.scale.resize(size);
		data.dirty.resize(size);
		data.rotation.resize(size);
		data.worldMatrix.resize(size);
		data.localMatrix.resize(size);
		data.entityId.resize(size);
		data.parent.resize(size);
		data.childs.resize(size);

		for (size_t i = 0; i < size; ++i)
		{
			data.entityMap[i] = i;
		}
	}

	void setLocation(TransformData& data, EntityID entityID, const glm::vec3& position)
	{
		auto iter = data.entityMap.find(entityID);
		if (iter == data.entityMap.end())
		{
			return;
		}
		size_t index = iter->second;
		data.dirty[index] = true;
		data.location[index] = position;
	}

	void setRotation(TransformData& data, EntityID entityID, const glm::quat& rotation)
	{
		auto iter = data.entityMap.find(entityID);
		if (iter == data.entityMap.end())
		{
			return;
		}
		size_t index = iter->second;
		data.dirty[index] = true;
		data.rotation[index] = rotation;
	}

	void setScale(TransformData& data, EntityID entityID, const glm::vec3& scale)
	{
		auto iter = data.entityMap.find(entityID);
		if (iter == data.entityMap.end())
		{
			return;
		}
		size_t index = iter->second;
		data.dirty[index] = true;
		data.scale[index] = scale;
	}


	void runData(Timings& timings, size_t element_count, std::function<void(std::vector<TestBed::Transform>&)> reset)
	{
		size_t iteration_count = timings.size();
		std::vector<glm::mat4> localMatrix;
		localMatrix.resize(element_count);

		std::vector<TestBed::Transform> transforms;
		transforms.resize(element_count);

		for (size_t iter = 0; iter < iteration_count; ++iter)
		{
			reset(transforms);
			auto start_time = Clock::now();

			for (size_t i = 0; i < element_count; ++i)
			{
				auto& transform = transforms[i];
				if (transform.dirty)
				{
					glm::mat4 mat(1.0f);
					glm::mat4 matrix = glm::translate(mat, transform.location);
					matrix *= glm::toMat4(transform.rotation);
					matrix *= glm::scale(mat, transform.scale);
					transform.dirty = false;

					localMatrix[i] = matrix;
				}
			}

			auto end_time = Clock::now();
			timings[iter] = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
		}
	}

	void runData(Timings& timings, size_t element_count, std::function<void(TestBed::TransformData&)> reset)
	{
		size_t iteration_count = timings.size();

		TestBed::TransformData data;
		TestBed::resize(data, element_count);
		for (size_t iter = 0; iter < iteration_count; ++iter)
		{
			reset(data);
			auto start_time = Clock::now();

			for (size_t i = 0; i < element_count; ++i)
			{
				if (data.dirty[i])
				{
					glm::mat4 mat(1.0f);
					glm::mat4 matrix = glm::translate(mat, data.location[i]);
					matrix *= glm::toMat4(data.rotation[i]);
					matrix *= glm::scale(mat, data.scale[i]);
					data.dirty[i] = false;

					data.localMatrix[i] = matrix;
				}
			}

			auto end_time = Clock::now();
			timings[iter] = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
		}
	}

} // ns TestBed
