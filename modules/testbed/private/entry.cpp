#include <testbed/entry.h>
#include <algorithm>
#include <testbed/util.h>

namespace TestBed
{

Entry::Entry(const std::string& name, const std::string& type, Timings& timings)
: m_data(timings)
, m_sorted(timings)
, m_name(name)
, m_type(type)
{
	std::sort(m_sorted.begin(), m_sorted.end());
}

void Entry::addMetadata(const std::string& key, const std::string& data)
{
	m_meta[key] = data;
}

void Entry::setThreadCount(size_t count)
{
	m_threadCount = count;
}

void Entry::setThreadID(size_t count)
{
	m_threadID = count;
}

void Entry::setAffinity(size_t affinity)
{
	m_threadAffinity = affinity;
}


const std::map<std::string, std::string>& Entry::getMetadata() const
{
	return m_meta;
}

const std::string& Entry::getName() const
{
	return m_name;
}

const std::string& Entry::getDatatype() const
{
	return m_type;
}

size_t Entry::getSize() const
{
	return m_data.size();
}

size_t Entry::getThreadCount() const
{
	return m_threadCount;
}

size_t Entry::getThreadID() const
{
	return m_threadID;
}

size_t Entry::getAffinity() const
{
	return m_threadAffinity;
}

const Timings& Entry::data() const
{
	return m_data;
}

double Entry::getAverage() const
{
	if (m_data.empty())
	{
		return 0.0;
	}
	double val = 0;
	for (size_t i = 0; i < m_data.size(); ++i)
	{
		val += m_data[i];
	}
	return val / m_data.size();
}

double Entry::getPercentile(float number) const
{
	if (number > 100.0f)
	{
		number = 100.0f;
	}
	if (number < 0.0f)
	{
		number = 0.0f;
	}
	float normal = number / 100.0f;
	float index = normal * (float)m_data.size();
	size_t a = (size_t)index;
	size_t b = a + 1;

	float fa = index - (a / (float)m_data.size());
	float fb = (b / (float)m_data.size()) - index;


	return 1.0;
}

std::string Entry::toString() const
{
	std::string str;

	str += getName() + "\n";

	for (auto& iter : m_meta)
	{
		str += " " + iter.first + ": " + iter.second + "\n";
	}
	str += " " + std::to_string(getAverage()) + " " + getDatatype();

	return str;
}

} // ns TestBed
