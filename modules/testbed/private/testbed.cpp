#include <testbed/testbed.h>
#include <iostream>

#include <testbed/types.h>
#include <testbed/cachetest.h>
#include <testbed/result.h>
#include <testbed/random.h>
#include <testbed/util.h>
#include <future>
#include <thread>

#include <fstream>
#include <sqlite3.h>

namespace TestBed
{
	void test(Result& result, size_t element_count, size_t thread_count, bool affinity_enabled)
	{
		auto maxthreads = std::thread::hardware_concurrency();
		if (maxthreads < 1)
		{
			maxthreads = 1;
		}
		{
			std::cout << "Starting Array of structs. elements: " << element_count << " threads: " << thread_count << std::endl;
			std::promise<bool> ready;
			std::vector<std::thread> threads;
			threads.resize(thread_count);
			auto future = ready.get_future();
			for (size_t tid = 0; tid < thread_count; ++tid)
			{
				threads[tid] = std::thread([&future, &result, tid, element_count, thread_count, maxthreads, affinity_enabled]() {
					future.wait();

					Timings timings;
					timings.resize(element_count);

					runData(
						timings,
						element_count,
						[](std::vector<Transform>& data) {
						Random random(0, 100);
						Random sclrnd(-1, 2);

						for (auto& transform : data)
						{
							transform.location = glm::vec3(random.get(), random.get(), random.get());
							transform.scale = glm::vec3(sclrnd.get(), sclrnd.get(), sclrnd.get());
							transform.rotation = glm::quat(random.get(), random.get(), random.get(), random.get());
							transform.dirty = true;
						}
					});

					auto& entry = result.create("array of structs", "microseconds", timings);
					entry.setThreadCount(thread_count);
					entry.setThreadID(tid);
					if (affinity_enabled)
					{
						entry.setAffinity(tid % maxthreads);
					}
				});
				if (affinity_enabled)
				{
					setAffinity(threads[tid], tid % maxthreads);
				}
			}
			ready.set_value(true);

			for (size_t tid = 0; tid < thread_count; ++tid)
			{
				threads[tid].join();
			}
		}

		{
			std::cout << "Starting Struct of arrays. elements: " << element_count << " threads: " << thread_count << std::endl;
			std::promise<bool> ready;
			std::vector<std::thread> threads;
			threads.resize(thread_count);
			auto future = ready.get_future();
			for (size_t tid = 0; tid < thread_count; ++tid)
			{
				threads[tid] = std::thread([&future, &result, tid, element_count, thread_count, maxthreads, affinity_enabled]() {
					future.wait();

					Timings timings;
					timings.resize(element_count);

					runData(
						timings,
						element_count,
						[](TransformData& data) {
						size_t count = data.entityId.size();
						Random random(0, 100);
						Random sclrnd(-1, 2);

						for (size_t i = 0; i < count; ++i)
						{
							setLocation(data, i, glm::vec3(random.get(), random.get(), random.get()));
							setScale(data, i, glm::vec3(sclrnd.get(), sclrnd.get(), sclrnd.get()));
							setRotation(data, i, glm::quat(random.get(), random.get(), random.get(), random.get()));
						}
					});
					auto& entry = result.create("struct of arrays", "microseconds", timings);
					entry.setThreadCount(thread_count);
					entry.setThreadID(tid);
					if (affinity_enabled)
					{
						entry.setAffinity(tid % maxthreads);
					}
				});
				if (affinity_enabled)
				{
					setAffinity(threads[tid], tid % maxthreads);
				}
			}
			ready.set_value(true);

			for (size_t tid = 0; tid < thread_count; ++tid)
			{
				threads[tid].join();
			}
		}
	}

	void cacheTest(size_t start, size_t end, size_t jump)
	{
		Result result;
		result.addMetadata("precision (s.)", toString((double)Clock::period::num / (double)Clock::period::den));
		result.addMetadata("core count", std::to_string(std::thread::hardware_concurrency()));

		// with affinity
		for (size_t element_count = 1; element_count <= 10; ++element_count)
		{
			for (size_t thread_count = 1; thread_count <= std::thread::hardware_concurrency(); ++thread_count)
			{
				test(result, element_count * 1000, thread_count, true);
			}
		}
		// with random
		for (size_t element_count = 1; element_count <= 10; ++element_count)
		{
			for (size_t thread_count = 1; thread_count <= std::thread::hardware_concurrency(); ++thread_count)
			{
				test(result, element_count * 1000, thread_count, false);
			}
		}

		std::cout << "Results:" << std::endl;

		auto unixtime = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		std::string filename = std::to_string(unixtime);

		std::cout << filename << std::endl;

		std::ofstream file;
		file.open(filename + ".json");
		file << result.get(SerializationFormat::Json);
		file.close();

		file.open(filename + ".csv");
		file << result.get(SerializationFormat::Csv);
		file.close();

		{
			// sqlite
			sqlite3 *db;
			int rc;

			std::string fname = filename + ".db";
			rc = sqlite3_open(fname.c_str(), &db);

			if (rc == 0)
			{
				if (!result.serialize(db))
				{
					std::cout << "Failed to serialize results into: " << fname << std::endl;
				}
			}
			else
			{
				std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
			}

			sqlite3_close(db);
		}
	}
} // ns TestBed
