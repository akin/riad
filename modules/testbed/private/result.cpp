#include <testbed/result.h>
#include <thread>
#include <testbed/util.h>
#include <sqlite3.h>
#include <fmt/format.h>
#include <iostream>

namespace TestBed
{
Entry& Result::create(const std::string& name, const std::string& type, Timings& timings)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_entries.emplace_back(name, type, timings);
	Entry& entry = m_entries.back();
	return entry;
}

void Result::addMetadata(const std::string& key, const std::string& data)
{
	m_meta[key] = data;
}

namespace
{
	namespace json
	{
		void add(std::string& input, const std::string& key, const std::string& value)
		{
			if (value.empty())
			{
				return;
			}
			input += "\"" + key + "\": \"" + value + "\",\n";
		}
		void add(std::string& input, const std::string& key, size_t value)
		{
			if (value == std::numeric_limits<size_t>::max())
			{
				return;
			}
			input += "\"" + key + "\": " + std::to_string(value) + ",\n";
		}

		std::string serialize(const Entry& entry)
		{
			std::string str;
			str += "{\n";

			auto& meta = entry.getMetadata();
			if (meta.size() > 0)
			{
				size_t count = 0;
				str += "\"meta\": {\n";
				for (auto iter = meta.begin(); iter != meta.end(); ++iter)
				{
					++count;
					str += "\"" + iter->first + "\": \"" + iter->second + "\"";
					if (count < meta.size())
					{
						str += ",\n";
					}
					else
					{
						str += "\n";
					}
				}
				str += "},\n";
			}
			auto& data = entry.data();
			add(str, "average", entry.getAverage());
			add(str, "type", entry.getDatatype());
			add(str, "name", entry.getName());
			add(str, "element count", entry.getSize());
			add(str, "thread count", entry.getThreadCount());
			add(str, "thread id", entry.getThreadID());
			add(str, "affinity", entry.getAffinity());

			str += "\"data\": [\n";
			for (size_t i = 0; i < data.size(); ++i)
			{
				str += ::TestBed::toString(data[i]);
				if (i < data.size() - 1)
				{
					str += ",";
				}
				else
				{
					str += "";
				}
			}
			str += "]\n";
			str += "}";

			return str;
		}
	}
	namespace csv
	{
		void add(std::string& input, const std::string& value)
		{
			if (!value.empty())
			{
				input += value;
			}
			input += ";";
		}
		void add(std::string& input, size_t value)
		{
			if (value != std::numeric_limits<size_t>::max())
			{
				input += std::to_string(value);
			}
			input += ";";
		}

		std::string serializeHeader()
		{
			std::string str;
			add(str, "name");
			add(str, "element count");
			add(str, "thread count");
			add(str, "affinity");
			add(str, "thread id");
			add(str, "average");
			add(str, "type");

			str += "\n";

			return str;
		}

		std::string serialize(const Entry& entry)
		{
			std::string str;
			add(str, entry.getName());
			add(str, entry.getSize());
			add(str, entry.getThreadCount());
			add(str, entry.getAffinity());
			add(str, entry.getThreadID());
			add(str, entry.getAverage());
			add(str, entry.getDatatype());

			str += "\n";

			return str;
		}
	}
	namespace sql
	{
		std::string toString(Type type)
		{
			switch (type)
			{
			case Type::boolean:
			case Type::uint32:
			case Type::uint64:
			case Type::int32:
			case Type::int64:
				return "INTEGER";
			case Type::float32:
			case Type::float64:
				return "REAL";
			case Type::string:
				return "TEXT";
			default:
				assert(false);
			}
			return "";
		}

		bool tableExists(sqlite3* db, const std::string& table)
		{
			if (table.empty())
			{
				return false;
			}
			// Create SQL statement 
			std::string query = fmt::format("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{0}';", table);
			// Execute SQL statement 
			char *err = 0;
			bool data = false;
			int rc = sqlite3_exec(db, query.c_str(), [](void *data, int argc, char **argv, char **azColName) -> int {
				bool *value = (bool*)data;
				*value = argc == 1;
				return 0;
			}, 
			(void*)&data,
			&err);

			if (rc != SQLITE_OK) 
			{
				//fprintf(stderr, "SQL error: %s\n", err);
				sqlite3_free(err);
				return false;
			}
			return data;
		}

		bool tableCreate(sqlite3* db, const std::string& table, std::vector<NamedType> types)
		{
			if (types.empty())
			{
				return true;
			}

			std::string query = fmt::format("CREATE TABLE {0}(", table);
			query += "ID INTEGER PRIMARY KEY";
			for (auto& type : types)
			{
				query += fmt::format(", {0} {1}", type.name, toString(type.type));
			}
			query += ");";

			// Execute SQL statement 
			char *err = 0;
			int rc = sqlite3_exec(db, query.c_str(), [](void *data, int argc, char **argv, char **azColName) -> int {
				return 0;
			},
				nullptr,
				&err);

			if (rc != SQLITE_OK)
			{
				//fprintf(stderr, "SQL error: %s\n", err);
				sqlite3_free(err);
				return false;
			}
			return true;
		}

		int64_t tableInsert(sqlite3* db, const std::string& table, const std::string& structure, const std::string& value)
		{
			if (value.empty())
			{
				return -1;
			}

			std::string query = fmt::format("INSERT INTO {0} ({1}) VALUES ({2});", table, structure, value);

			// Execute SQL statement 
			char *err = 0;
			int rc = sqlite3_exec(db, query.c_str(), [](void *data, int argc, char **argv, char **azColName) -> int {
				int i;
				for (i = 0; i<argc; i++) {
					printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
				}
				printf("\n");
				return 0;
			},
				nullptr,
				&err);

			if (rc != SQLITE_OK)
			{
				sqlite3_free(err);
				return -1;
			}
			int64_t rowid = sqlite3_last_insert_rowid(db);
			return rowid;
		}

		bool tableInsertMulti(sqlite3* db, const std::string& table, const std::string& structure, std::vector<std::string> values)
		{
			if (values.empty())
			{
				return true;
			}

			std::string query = fmt::format("INSERT INTO {0} ({1}) VALUES ", table, structure);

			size_t count = 0;
			for (auto& str : values)
			{
				query += "(" + str + ")";
				++count;
				if (count != values.size())
				{
					query += ", ";
				}
			}

			query += ";";

			// Execute SQL statement 
			char *err = 0;
			int rc = sqlite3_exec(db, query.c_str(), [](void *data, int argc, char **argv, char **azColName) -> int {
				int i;
				for (i = 0; i<argc; i++) {
					printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
				}
				printf("\n");
				return 0;
			},
				nullptr,
				&err);

			if (rc != SQLITE_OK)
			{
				sqlite3_free(err);
				return false;
			}
			return true;
		}
	}
}

std::string Result::get(SerializationFormat format) const
{
	std::lock_guard<std::mutex> lock(m_mutex);

	std::string str;
	if (format == SerializationFormat::Json)
	{
		str += "{\n";

		if (m_meta.size() > 0)
		{
			size_t count = 0;
			str += "\"meta\": {\n";
			for (auto iter = m_meta.begin(); iter != m_meta.end(); ++iter)
			{
				++count;
				str += "\"" + iter->first + "\": \"" + iter->second + "\"";
				if (count < m_meta.size())
				{
					str += ",\n";
				}
				else
				{
					str += "\n";
				}
			}
			str += "}\n";
		}
		if (m_entries.size() > 0)
		{
			size_t count = 0;
			str += ",\"entries\": [\n";
			for (auto iter = m_entries.begin(); iter != m_entries.end(); ++iter)
			{
				++count;
				str += json::serialize(*iter);
				if (count < m_entries.size())
				{
					str += ",\n";
				}
				else
				{
					str += "\n";
				}
			}
			str += "]\n";
		}
		str += "}";
	}
	else if (format == SerializationFormat::Csv)
	{
		if (m_meta.size() > 0)
		{
			std::string headers;
			std::string values;
			for (auto iter = m_meta.begin(); iter != m_meta.end(); ++iter)
			{
				csv::add(headers, iter->first);
				csv::add(values, iter->second);
			}
			str += headers + "\n";
			str += values + "\n";
		}
		str += "\n";
		str += csv::serializeHeader();

		for (auto iter = m_entries.begin(); iter != m_entries.end(); ++iter)
		{
			str += csv::serialize(*iter);
		}
	}
	return str;
}

bool Result::serialize(sqlite3 *db)
{
	if (db == nullptr)
	{
		return false;
	}

	// Insert metadata
	{
		if (!sql::tableExists(db, "metadata"))
		{
			if (!sql::tableCreate(db, "metadata", {
				{ "key", Type::string },
				{ "value", Type::string }
				})) {
				std::cout << "Failed to insert metadata table." << std::endl;
				return false;
			}
		}

		for (auto& iter : m_meta)
		{
			if (sql::tableInsert(db, "metadata", "key, value", fmt::format("'{0}', '{1}'", iter.first, iter.second)) < 0)
			{
				std::cout << "Failed to insert into metadata." << std::endl;
				return false;
			}
		}
	}

	// Insert data
	{
		if (!sql::tableExists(db, "result"))
		{
			if (!sql::tableCreate(db, "result", {
				{ "name", Type::string },
				{ "elements", Type::uint64 },
				{ "thread_count", Type::uint64 },
				{ "affinity", Type::uint64 },
				{ "thread", Type::uint64 },
				{ "average", Type::float64 },
				{ "type", Type::string }
				})) {
				std::cout << "Failed to insert result table." << std::endl;
				return false;
			}
		}
		if (!sql::tableExists(db, "timings"))
		{
			if (!sql::tableCreate(db, "timings", {
				{ "result", Type::int64 },
				{ "idx", Type::uint64 },
				{ "value", Type::uint64 }
				})) {
				std::cout << "Failed to insert timings table." << std::endl;
				return false;
			}
		}

		std::vector<std::string> values;
		for (auto& item : m_entries)
		{
			std::string affinity = "NULL";
			if (item.getAffinity() != std::numeric_limits<size_t>::max())
			{
				affinity = std::to_string(item.getAffinity());
			}
			int64_t iid = sql::tableInsert(
				db, 
				"result", 
				"name, elements, thread_count, affinity, thread, average, type", 
				fmt::format(
					"'{0}', {1}, {2}, {3}, {4}, {5}, '{6}'",
					item.getName(),
					item.getSize(),
					item.getThreadCount(),
					affinity,
					item.getThreadID(),
					item.getAverage(),
					item.getDatatype()
				)
			);

			if (iid < 0)
			{
				std::cout << "Failed to insert into result." << std::endl;
				return false;
			}

			std::vector<std::string> data;
			const auto& timings = item.data();
			for (size_t i = 0; i < timings.size(); ++i)
			{
				data.push_back(fmt::format("{0}, {1}, {2}", iid, i, timings[i]));
			}

			if (!sql::tableInsertMulti(
				db,
				"timings",
				"result, idx, value",
				data
			))
			{
				std::cout << "Failed to insert into timings." << std::endl;
				return false;
			}
		}
	}

	return true;
}

std::string Result::toString() const
{
	std::lock_guard<std::mutex> lock(m_mutex);

	std::string str;

	for (auto& iter : m_meta)
	{
		str += iter.first + ": " + iter.second + "\n";
	}
	str += "\n====\n";
	for (auto& item : m_entries)
	{
		str += item.toString() + "\n----\n";
	}

	return str;
}

} // ns TestBed
