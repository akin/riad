#include <testbed/util.h>
#include <sstream>

#if defined(__unix__)
# define USE_PTHREAD
#endif

#if defined(USE_PTHREAD)
# include <pthread.h>
#else
# include <Windows.h>
#endif

namespace TestBed
{

bool setAffinity(std::thread& thread, size_t core)
{
#if defined(USE_PTHREAD)
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core, &cpuset);
	int rc = pthread_setaffinity_np(thread.native_handle(), sizeof(cpu_set_t), &cpuset);
	return rc == 0;
#else
	return SetThreadAffinityMask(thread.native_handle(), 1 << core) != 0;
#endif
}

std::string toString(double number)
{
	std::ostringstream oss;
	oss << number;
	return oss.str();
}

} // ns TestBed
