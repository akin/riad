
#ifndef TESTBED_H_
#define TESTBED_H_

#include "types.h"
#include <functional>

namespace TestBed
{
	void cacheTest(size_t start = 1000, size_t end = 10000, size_t jump = 1000);
} // ns TestBed

#endif // TESTBED_H_
