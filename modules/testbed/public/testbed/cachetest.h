
#ifndef TESTBED_CACHETEST_H_
#define TESTBED_CACHETEST_H_

#include "types.h"
#include <functional>


namespace TestBed
{
	void resize(TransformData& data, size_t size);
	void setLocation(TransformData& data, EntityID entityID, const glm::vec3& position);
	void setRotation(TransformData& data, EntityID entityID, const glm::quat& rotation);
	void setScale(TransformData& data, EntityID entityID, const glm::vec3& scale);

	void runData(Timings& timings, size_t element_count, std::function<void(std::vector<TestBed::Transform>&)> reset);
	void runData(Timings& timings, size_t element_count, std::function<void(TestBed::TransformData&)> reset);

} // ns TestBed

#endif // TESTBED_CACHETEST_H_
