
#ifndef TESTBED_ENTRY_H_
#define TESTBED_ENTRY_H_

#include "types.h"
#include <string>
#include <map>

namespace TestBed
{
	class Entry
	{
	private:
		Timings m_data;
		Timings m_sorted;

		std::string m_name;
		std::string m_type;

		size_t m_threadCount = std::numeric_limits<size_t>::max();
		size_t m_threadID = std::numeric_limits<size_t>::max();
		size_t m_threadAffinity = std::numeric_limits<size_t>::max();

		std::map<std::string, std::string> m_meta;
	public:
		Entry(const std::string& name, const std::string& type, Timings& timings);

		void addMetadata(const std::string& key, const std::string& data);

		void setThreadCount(size_t count);
		void setThreadID(size_t count);
		void setAffinity(size_t affinity);

		const std::map<std::string, std::string>& getMetadata() const;
		const std::string& getName() const;
		const std::string& getDatatype() const;
		size_t getSize() const;

		size_t getThreadCount() const;
		size_t getThreadID() const;
		size_t getAffinity() const;

		const Timings& data() const;

		double getAverage() const;
		double getPercentile(float number) const;

		std::string toString() const;
	};
} // ns TestBed

#endif // TESTBED_ENTRY_H_
