
#ifndef TESTBED_RESULT_H_
#define TESTBED_RESULT_H_

#include "types.h"
#include "entry.h"
#include <list>
#include <mutex>
#include <map>

namespace TestBed
{
	class Result
	{
	private:
		mutable std::mutex m_mutex;
		std::list<Entry> m_entries;
		std::map<std::string, std::string> m_meta;
	public:
		Entry& create(const std::string& name, const std::string& type, Timings& timings);

		void addMetadata(const std::string& key, const std::string& data);

		std::string get(SerializationFormat format) const;
		bool serialize(sqlite3 *db);
		std::string toString() const;
	};
} // ns TestBed

#endif // TESTBED_RESULT_H_
