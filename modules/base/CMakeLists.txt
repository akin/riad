project(base C CXX)
cmake_minimum_required(VERSION 3.7)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

file(GLOB CURRENT_SOURCES 
	${CMAKE_CURRENT_LIST_DIR}/private/*.cpp 
	${CMAKE_CURRENT_LIST_DIR}/private/*.c
	${CMAKE_CURRENT_LIST_DIR}/private/*.inl
	${CMAKE_CURRENT_LIST_DIR}/private/*.h
	${CMAKE_CURRENT_LIST_DIR}/public/base/*.inl
	${CMAKE_CURRENT_LIST_DIR}/public/base/*.h
	${CMAKE_CURRENT_LIST_DIR}/public/graphics/*.inl
	${CMAKE_CURRENT_LIST_DIR}/public/graphics/*.h
	${CMAKE_CURRENT_LIST_DIR}/public/thread/*.inl
	${CMAKE_CURRENT_LIST_DIR}/public/thread/*.h
	${CMAKE_CURRENT_LIST_DIR}/interface/thread/*.inl
	${CMAKE_CURRENT_LIST_DIR}/interface/thread/*.h
)

add_library(
	${PROJECT_NAME} 
	STATIC 
	${CURRENT_SOURCES}
)

target_include_directories(
	${PROJECT_NAME} 
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/private"
	PUBLIC 
		"${CMAKE_CURRENT_LIST_DIR}/public"
)

set_target_properties(
	${PROJECT_NAME} 
	PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/public"
)

add_subdirectory("test")