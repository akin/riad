
#include <thread/tools.h>

#if defined(__unix__)
# define USE_PTHREAD
#endif

#if defined(USE_PTHREAD)
# include <pthread.h>
# else
# include <Windows.h>
#endif

namespace Thread {

// http://en.cppreference.com/w/cpp/thread/thread/native_handle
bool setPriority(std::thread& thread, size_t priority)
{
#if defined(USE_PTHREAD)
    sched_param sch;
    int policy; 
    pthread_getschedparam(thread.native_handle(), &policy, &sch);
    sch.sched_priority = priority;

    return pthread_setschedparam(thread.native_handle(), SCHED_FIFO, &sch) == 0;
#else
    return false;
#endif
}

size_t getPriority(std::thread& thread)
{
#if defined(USE_PTHREAD)
    sched_param sch;
    int policy; 
    pthread_getschedparam(thread.native_handle(), &policy, &sch);
    return sch.sched_priority;
#else
    return 0;
#endif
}

bool setAffinity(std::thread& thread, size_t core)
{
#if defined(USE_PTHREAD)
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core, &cpuset);
	int rc = pthread_setaffinity_np(thread.native_handle(), sizeof(cpu_set_t), &cpuset);
	return rc == 0;
#else
	return SetThreadAffinityMask(thread.native_handle(), 1 << core) != 0;
#endif
}











} // ns Thread
