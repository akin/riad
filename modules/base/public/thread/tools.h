
#ifndef BASE_THREADTOOLS
#define BASE_THREADTOOLS

#include <thread>

namespace Thread {

bool setPriority(std::thread& thread, size_t priority);
size_t getPriority(std::thread& thread);
bool setAffinity(std::thread& thread, size_t core);

} // ns Thread

#endif // BASE_THREADTOOLS