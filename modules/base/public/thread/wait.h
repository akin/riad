/**
 * Thread safe Waitlist.
 */
#ifndef BASE_THREADWAIT
#define BASE_THREADWAIT
 
#include <atomic>
#include <condition_variable>
#include <mutex>
 
namespace Thread {

class Wait
{
private:
    std::atomic_bool valid{true};
    mutable std::mutex mutex;
    std::condition_variable condition;
public:
    ~Queue()
    {
        invalidate();
    }

    /**
     * blocks until woken up..
     * nothing against spurious wakeups
    */
    bool wait()
    {
        std::unique_lock<std::mutex> lock{mutex};
        condition.wait(lock);

        if(!valid)
        {
            return false;
        }
        return true;
    }

    void release()
    {
        std::lock_guard<std::mutex> lock{mutex};
        condition.notify_all();
    }

    /**
    * Invalidate the queue.
    * Used to ensure no conditions are being waited on in waitPop when
    * a thread or the application is trying to exit.
    * The queue is invalid after calling this method and it is an error
    * to continue using a queue after this method has been called.
    */
    void invalidate()
    {
        std::lock_guard<std::mutex> lock{mutex};
        valid = false;
        condition.notify_all();
    }

    /**
    * Returns whether or not this queue is valid.
    */
    bool isValid() const
    {
        std::lock_guard<std::mutex> lock{mutex};
        return valid;
    }
};

} // ns Thread
 
#endif // BASE_THREADWAIT