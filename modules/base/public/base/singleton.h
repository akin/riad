
#ifndef BASE_SINGLETON_H_
#define BASE_SINGLETON_H_

namespace Base {

template <class CType>
CType& getSingleton()
{
    static CType instance;
    return instance;
}

} // ns Base

#endif // BASE_SINGLETON_H_
