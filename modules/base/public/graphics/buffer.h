
#ifndef BASE_GRAPHICS_BUFFER_H_
#define BASE_GRAPHICS_BUFFER_H_

#include <base/common.h>
#include <functional>
#include <vector>

namespace Graphics {

template <class Type>
class Buffer
{
    BASE_ClassDef(Buffer<Type>);
private:
    using BufferType = std::vector<Type>;

    BufferType m_buffer;
public:
    Buffer()
    {
    }

    Buffer(size_t size)
    {
        resize(size);
    }

    Buffer(const Buffer<Type>& other)
    {
        resize(other.size());
        memcpy(m_buffer.data(), other.data(), sizeBytes());
    }

    template <class Type2>
    Buffer(const Buffer<Type2>& other)
    {
        set<Type2>(other);
    }

    void resize(size_t size)
    {
        m_buffer.resize(size);
    }

    void clear()
    {
        m_buffer.swap(BufferType());
    }

    size_t sizeBytes() const
    {
        return m_buffer.size() * sizeof(Type);
    }

    size_t size() const
    {
        return m_buffer.size();
    }

    const Type *data() const
    {
        return m_buffer.data();
    }

    Type *data()
    {
        return m_buffer.data();
    }

    void set(const Buffer<Type>& other)
    {
        size_t size = other.size();
        resize(size);
        memcpy(m_buffer.data(), other.data(), sizeBytes());
    }

    template <class Type2>
    void set(const Buffer<Type2>& other)
    {
        size_t size = other.size();
        resize(size);

        const auto source = other.data();
        auto target = data();

        for(size_t i = 0 ; i < size ; ++i)
        {
            target[i] = source[i];
        }
    }
};

} // ns Graphics

#endif // BASE_GRAPHICS_BUFFER_H_
