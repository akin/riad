
#ifndef BASE_GRAPHICS_TEXTURE3D_H_
#define BASE_GRAPHICS_TEXTURE3D_H_

#include <base/common.h>
#include <functional>
#include <graphics/buffer.h>

namespace Graphics {

template <class Type>
class Texture3D
{
    BASE_ClassDef(Texture3D<Type>);
private:
    size_t m_width = 0;
    size_t m_height = 0;
    size_t m_depth = 0;

    Buffer<Type> m_buffer;
public:
    Texture3D();
    Texture3D(size_t width, size_t height, size_t depth);
    Texture3D(const Texture3D<Type>& other);
    template <class Type2> Texture3D(const Texture3D<Type2>& other);

    void resize(size_t width, size_t height, size_t depth);

    void clear();

    size_t getWidth() const;
    size_t getHeight() const;
    size_t getDepth() const;

    size_t sizeBytes() const;
    size_t size() const;

    const Buffer<Type>& getBuffer() const;

    const Type *data() const;
    Type *data();

    Type& at(size_t width, size_t height, size_t depth);

    void set(const Texture3D<Type>& other);
    template <class Type2> void set(const Texture3D<Type2>& other);
    
    void filter(const std::function<void(Type& pixel)>& func);
    void filter(const std::function<void(size_t x, size_t y, size_t z, Type& pixel)>& func);
};

template <class T>
Texture3D<T>::Texture3D()
{
}

template <class T>
Texture3D<T>::Texture3D(size_t width, size_t height, size_t depth)
{
    resize(width, height, depth);
}

template <class T>
Texture3D<T>::Texture3D(const Texture3D<T>& other)
{
    m_width = other.getWidth();
    m_height = other.getHeight();
    m_depth = other.getDepth();
    m_buffer.set(other.getBuffer());
}

template <class T>
template <class Type2>
Texture3D<T>::Texture3D(const Texture3D<Type2>& other)
{
    set<Type2>(other);
}

template <class T>
void Texture3D<T>::resize(size_t width, size_t height, size_t depth)
{
    m_width = width;
    m_height = height;
    m_depth = depth;
    m_buffer.resize(width * height * depth);
}

template <class T>
void Texture3D<T>::clear()
{
    m_width = 0;
    m_height = 0;
    m_depth = 0;
    m_buffer.clear();
}

template <class T>
size_t Texture3D<T>::getWidth() const
{
    return m_width;
}

template <class T>
size_t Texture3D<T>::getHeight() const
{
    return m_height;
}

template <class T>
size_t Texture3D<T>::getDepth() const
{
    return m_depth;
}

template <class T>
size_t Texture3D<T>::sizeBytes() const
{
    return m_buffer.size() * sizeof(T);
}

template <class T>
size_t Texture3D<T>::size() const
{
    return m_buffer.size();
}

template <class T>
const Buffer<T>& Texture3D<T>::getBuffer() const
{
    return m_buffer;
}

template <class T>
const T *Texture3D<T>::data() const
{
    return m_buffer.data();
}

template <class T>
T *Texture3D<T>::data()
{
    return m_buffer.data();
}

template <class T>
T& Texture3D<T>::at(size_t width, size_t height, size_t depth)
{
    assert(m_width > width || m_height > height || m_depth > depth);
    auto *data = m_buffer.data();
    return data[(depth * m_height * m_width) + (height * m_width) + width];
}


template <class T>
void Texture3D<T>::set(const Texture3D<T>& other)
{
    m_width = other.getWidth();
    m_height = other.getHeight();
    m_depth = other.getDepth();
    m_buffer.set(other.getBuffer());
}

template <class T>
template <class Type2>
void Texture3D<T>::set(const Texture3D<Type2>& other)
{
    m_width = other.getWidth();
    m_height = other.getHeight();
    m_depth = other.getDepth();
    m_buffer.template set<Type2>(other.getBuffer());
}

template <class T>
void Texture3D<T>::filter(const std::function<void(T& pixel)>& func)
{
    auto target = data();
    size_t size = this->size();
    for(size_t i = 0 ; i < size ; ++i)
    {
        func(target[i]);
    }
}

template <class T>
void Texture3D<T>::filter(const std::function<void(size_t x, size_t y, size_t z, T& pixel)>& func)
{
    auto target = data();
    size_t zoff = 0;
    size_t yoff = 0;
    for(size_t z = 0 ; z < m_depth ; ++z)
    {
        zoff = z * m_height * m_width;
        for(size_t y = 0 ; y < m_height ; ++y)
        {
            yoff = zoff + y * m_width;
            for(size_t x = 0 ; x < m_width ; ++x)
            {
                func(x,y,z, target[yoff + x]);
            }
        } 
    }
}

} // ns Graphics

#endif // BASE_GRAPHICS_TEXTURE3D_H_
