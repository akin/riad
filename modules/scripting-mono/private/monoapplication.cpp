
#include "monoapplication.h"
#include <iostream>
#include <vector>
#include "capi.h"

#ifdef EXAMPLE

#include <future>

#include "scripting.mono.h"

MonoApplication::MonoApplication()
: Base::Application("MonoApplication")
{
}

MonoApplication::~MonoApplication()
{
}

bool MonoApplication::init()
{
    return true;
}

int MonoApplication::run()
{
	Scripting::Mono script;

	if(!script.init())
	{
		std::cerr << "Failed to init scripting" << std::endl;
		return EXIT_FAILURE;
	}

    std::string path = "scripts.mono";
#ifdef __APPLE__
    // TODO, this is realy stupid.. 
    path = "/Users/akin/dev/riad/build/" + path;
#endif
	if(!script.load(path.c_str()))
	{
		std::cerr << "Failed to load script" << std::endl;
		return EXIT_FAILURE;
	}

    std::vector<const char*> args = {"first", "second"};
    int ret = script.run(args.size(), args.data());

	return ret;
}
#endif