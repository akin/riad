
#ifndef XCLIENT_BASE_H_
#define XCLIENT_BASE_H_

#include "xhttp.h"
#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <memory>
#include <asio.hpp>
#include "xhttpparser.h"
#include "xrequest.h"
#include "xmisc.h"

namespace xhttp {

// lazymode
using asio::ip::tcp;

class Package
{
public:
	const Request& request;

	asio::streambuf outStream;
	asio::streambuf inStream;

	size_t total_out = 0;
	size_t total_in = 0;
public:
    bool disconnectOnEOF = false;
public:
	Package(const Request& request)
	: request(request)
	{}
};

template <class TSocket>
class ClientBase : public Client
{
private:
	HTTPParser m_parser;
    TSocket &m_socket;
protected:
    std::string m_server;
	tcp::resolver m_resolver;
public:
	ClientBase(asio::io_context& io_context, TSocket& socket, const std::string& server)
	: m_socket(socket) // We get address, but socket _is__not_ initialized yet!!
    , m_server(server)
	, m_resolver(io_context)
	{
	}

	virtual ~ClientBase()
	{
	}
    
    void debugPrint(const char *data, size_t size)
    {
        std::vector<char> buffer(size + 1);
        memcpy(buffer.data(), data, size);
        buffer[size] = '\0';
        std::cout << buffer.data() << std::endl;
    }

	void send(const Request& config) final
	{
		auto package = std::make_shared<Package>(config);
		std::ostream request_stream(&(package->outStream));
		m_parser.createRequest(request_stream, m_server, config);

        if(!isOpen())
        {
            setStatus(Status::error, "Connection in error.");
            return;
        }
        
        {
            // Copy stuff from send for cache.
            package->disconnectOnEOF = package->request.keepAlive == 0;
        }

		asio::async_write(m_socket, package->outStream, [this, package](const asio::error_code& error, std::size_t bytes_transferred)
		{
			package->total_out += bytes_transferred;
			if(error)
			{
				setStatus(Status::error, error.message());
				return;
			}

			handleResponse(package);
		});
	}

	void handleResponse(std::shared_ptr<Package> package)
	{
		asio::async_read(m_socket, package->inStream, asio::transfer_at_least(1), [this, package](const asio::error_code& error, std::size_t bytes_transferred)
		{
			package->total_in += bytes_transferred;
			size_t count = package->inStream.size();

            if (!error)
            {
                m_parser.response(RequestStatus::success, package->request, asio::buffer_cast<const char*>(package->inStream.data()), count);
				package->inStream.consume(count);
            }
            else if (error == asio::error::eof || error == asio::error::connection_reset)
            {
                m_parser.response(RequestStatus::completed, package->request, asio::buffer_cast<const char*>(package->inStream.data()), count);
                close();
				package->inStream.consume(count);
                return;
            }
            else
            {
                setStatus(Status::error, error.message());
				return;
            }
            
            handleResponse(package);
		});
	}
};

} // NS xhttp

#endif // XCLIENT_BASE_H_
