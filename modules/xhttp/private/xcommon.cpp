
#include "xcommon.h"
#include <algorithm>

namespace xhttp {

void toLower(std::string& value)
{
    // lowercase the keys..
    std::for_each(value.begin(), value.end(), [](char & c) {
        c = ::tolower(c);
    });
}

} // ns xhttp