
#ifndef XHTTP_COMMON
#define XHTTP_COMMON

#include <string>

namespace xhttp {

void toLower(std::string& value);

} // ns xhttp

#endif
