
#ifndef XHTTP_CLIENT_H_
#define XHTTP_CLIENT_H_

#include <functional>
#include <string>
#include "xmisc.h"

namespace xhttp {

class Request;
class Client
{
public:
	using ConnectionCallback = std::function<void(Status status, const std::string& msg)>;
protected:
	ConnectionCallback m_connectioncb;
	Status m_status = Status::disconnected;

	void setStatus(Status status);
	void setStatus(Status status, const std::string& msg);
	void onCommunicationError(const std::string& msg);

	Client();
public:
	virtual ~Client() = 0;

	Status getStatus() const;

	void onConnection(const ConnectionCallback& callback);

    virtual bool isOpen() = 0;
	virtual void open() = 0;
	virtual void close() = 0;

	virtual void send(const Request& config) = 0;
};

} // ns xhttp

#endif // XHTTP_CLIENT_H_
