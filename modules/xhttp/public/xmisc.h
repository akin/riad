
#ifndef XHTTP_MISC_H_
#define XHTTP_MISC_H_

namespace xhttp {

enum ConnectionType
{
    http,
    https
};
    
enum class Status
{
	error = 0,
	success = 1,
	connecting = 2,
	connected = 3,
	disconnected = 4
};

enum class RequestStatus
{
	error = 0,
	success = 1,
	completed = 2
};

enum class RequestType {
    get,
    post,
    put,
    del
};

const char *toString(Status status);
const char *toString(RequestType type);

} // NS xhttp

#endif // XHTTP_MISC_H_
