
#ifndef XHTTP_REQUEST_H_
#define XHTTP_REQUEST_H_

#include <unordered_map>
#include <functional>
#include <string>
#include "xmisc.h"

namespace xhttp {

using ResponseCallback = std::function<void(RequestStatus status, const void *data, size_t size)>;
using DataCallback = std::function<size_t(size_t at, void *buffer, size_t size)>;
class Request
{
    friend class HTTPParser;
private:
    std::unordered_map<std::string, std::string> m_headers;

    std::string m_contentType;
    const void *m_data = nullptr;
    size_t m_size = 0;
    DataCallback m_dataCallback;

    ResponseCallback m_onResponse;
public:
	void onResponse(const ResponseCallback& callback);

    void responseData(RequestStatus status, const char* data, size_t size) const;
public:
    void setAdditionalHeader(const std::string& key, const std::string& value);
    bool getAdditionalHeader(const std::string& key , std::string& value) const;

    void setData(const std::string& contentType, const void *data, size_t size);
    void setData(const std::string& contentType, DataCallback& dataCallback, size_t size);

    /*
    setData("text/html", [](size_t at, size_t bufferSize void *buffer) -> size_t {
        // We are at AT
        // the buffer is size of size
        // buffer 
    }, 1000);
    */
    RequestType type = RequestType::get;

    std::string path = "/";
    
    size_t keepAlive = 0;

    // text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8
    std::string accept; 

    // Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0
    std::string userAgent;
};

} // ns xhttp

#endif // XHTTP_REQUEST_H_
