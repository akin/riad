

# XHTTP
 *  simple HTTP/HTTPS client wrapper for asio
 *  Generic use case: send json to server, receive json response.
 *  CMake
 *  C++11 (not limited, even 14/17/19 is accepted)
 *  static linking, no bazillion dependencies (no Boost!).
   *  asio (https://github.com/chriskohlhoff/asio.git)
   *  openssl

## Reasoning
*  Programs needs simple HTTP library, just for simple get/post requests, without anything complex
*  The only libraries that I've encountered
  * Platform specific
  * Dependencies (Boost)
  * Exceptions needed
  * No cmake support

## Current state
Currently in state of "dont give a ..." (burned my will to live A bit too much), the ssl integration was/is horrible mess
The Transport Level Security (TLS;SSL) is implemented, but not tested at all, the google server
which against, I tried to develop, just kept saying "wrong version".. thing is, I do not have proper
need for this library at the moment, so All my tests would be artificial.
Ill leave the library to "compiles, does not crash", state, until I need it.

## TODO
 *  Connection Keep Alive respect! & Streaming!
 *  List of usecases
 *  More testcases
 *  Context class needs to be thought through with multiple usecases (start, stop, pause, run until. eg.)
 *  Clean up & Refactor HTTPParser interface & class.. its trash atm
 *  Better Request generation interfaces
 *  Simple usecases
 *  Advanced streaming usecases