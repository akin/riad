
#ifndef VISO_DEV_TEST_H_
#define VISO_DEV_TEST_H_

#include <iostream>
#include "catch.hpp"

#include <viso/application.h>
#include <future>

TEST_CASE( "Simple Start viso.", "[]" ) {
    Viso::Application application;
    
    application.parseArgs(0, nullptr);
    if(!application.init())
    {
    }
    application.run();
}

#endif // VISO_DEV_TEST_H_
