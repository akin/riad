
#ifndef VISO_RECORDER_H_
#define VISO_RECORDER_H_

#include <viso/ray.h>
#include <viso/scene/camera.h>
#include <graphics/color.h>
#include <graphics/texture2d.h>
#include <interface/task.h>
#include <interface/taskmanager.h>
#include <thread/pool.h>
#include <base/taskmanager.h>
#include <base/lambdatask.h>
#include <functional>
#include "renderblock.h"
#include "renderdomain.h"
#include "common.h"
#include <future>

#include <base/log.h>

namespace Viso {

template <class TColor>
class Recorder
{
private:
    Graphics::Texture2D<TColor> m_texture;
    float m_step = 0.01f; // 0.001f is just random !TODO, someday figure out
    size_t m_taskCount = 2;
    size_t m_blockSize = 64;
    Base::TaskManager m_taskmanager;
    Thread::Pool m_threads;
public:
    Recorder();

    size_t getWidth() const;
    size_t getHeight() const;

    void setTaskCount(size_t count)
    {
        m_taskCount = count;
    }

    size_t getTaskCount() const
    {
        return m_taskCount;
    }
    
    void setup(size_t width, size_t height, float step = 0.001f);
    void record(const Camera& camera, Interface::TaskManager& tasker, const std::function<void(const Ray& ray, TColor& out)>& tracer);

    const Graphics::Texture2D<TColor>& getTexture() const;
};

template <class TColor>
Recorder<TColor>::Recorder()
: m_threads(m_taskmanager)
{
}

template <class TColor>
size_t Recorder<TColor>::getWidth() const
{
    return m_texture.getWidth();
}

template <class TColor>
size_t Recorder<TColor>::getHeight() const
{
    return m_texture.getHeight();
}

template <class TColor>
void Recorder<TColor>::setup(size_t width, size_t height, float step)
{
    m_step = step;
    m_texture.resize(width, height);
}

class SharedRenderData
{
public:
    std::vector<RenderBlock> blocks;
    std::atomic<size_t> iterator;
    RenderDomain domain;
};

template <class TColor>
void Recorder<TColor>::record(const Camera& camera, Interface::TaskManager& tasker, const std::function<void(const Ray& ray, TColor& out)>& tracer)
{
    // Take camera, NEAR, calculate the NEAR pyramid, and shrecordoot rays. accordingly.
    // Camera modelview also affects where the beams go..
    const float width = m_texture.getWidth();
    const float height = m_texture.getHeight();

    SharedRenderData data;
    data.domain.setup(
        width,
        height,
        m_step,
        camera.getNear(),
        glm::inverse(camera.getProjection()),
        camera.getTransform()
    );

    {
        RenderBlock block;
        auto& rect = block.getRectangle();
        rect.setHeight(m_blockSize);
        for(size_t y = 0 ; y < height ; y += m_blockSize)
        {
            rect.setY(y);
            if(rect.getY2() > height)
            {
                rect.setY2(height);
            }

            rect.setWidth(m_blockSize);
            for(size_t x = 0 ; x < width ; x += m_blockSize)
            {
                rect.setX(x);
                if(rect.getX2() > width)
                {
                    rect.setX2(width);
                }
                data.blocks.push_back(block);
            }
        }
    }
    
    auto promise = std::promise<bool>();
    auto future = promise.get_future();
    auto& buffer = m_texture;
    
    // The otherway to track counter, would be to ++counter; in for loop
    // unfortunately, that will create bugs, as if the first 2 threads run data through
    // before 3rd gets started, the promise will then be called twice!
    // oddities of threading..
    size_t numberOfTaskers = 3;
    std::atomic_size_t counter{numberOfTaskers};
    for(size_t i = 0 ; i < numberOfTaskers ; ++i)
    {
        auto sharedtask = Interface::Task::Shared(new Base::LambdaTask{[&data, &buffer, &tracer, &promise, &counter](){
            while(true)
            {
                size_t iter = data.iterator++;
                if(iter >= data.blocks.size())
                {
                    size_t count = --counter;
                    
                    if(count == 0)
                    {
                        promise.set_value(true);
                    }
                    return;
                }
                
                const auto& block = data.blocks[iter];
                const auto& rect = block.getRectangle();
                
                Ray ray;
                for(size_t y = rect.getY() ; y < rect.getY2() ; ++y)
                {
                    for(size_t x = rect.getX() ; x < rect.getX2() ; ++x)
                    {
                        data.domain.get(x, y, ray.position, ray.direction);
                        auto& pixel = buffer.at(x,y);
                        tracer(ray, pixel);
                    }
                }
            }
        }});
        
        m_taskmanager.add(sharedtask);
    }
    
    future.wait();
}

template <class TColor>
const Graphics::Texture2D<TColor>& Recorder<TColor>::getTexture() const
{
    return m_texture;
}

} // ns Viso

#endif // VISO_RECORDER_H_
