
#ifndef VISO_EVENT_H_
#define VISO_EVENT_H_

#include "../common.h"

namespace Viso {

class Event
{
private:
    Time m_time = 0;
public:
    virtual ~Event();

    Time getTime() const;
    void setTime(Time time);

    virtual void apply() = 0;
};

} // ns Viso

#endif // VISO_EVENT_H_
