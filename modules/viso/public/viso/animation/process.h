
#ifndef VISO_PROCESS_H_
#define VISO_PROCESS_H_

#include "../common.h"

namespace Viso {

class Process
{
public:
    virtual ~Process();

    virtual void update(Time time) = 0;
};

} // ns Viso

#endif // VISO_PROCESS_H_
