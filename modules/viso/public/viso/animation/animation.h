
#ifndef VISO_ANIMATION_H_
#define VISO_ANIMATION_H_

#include <vector>
#include <viso/scene/node.h>
#include "../common.h"
#include "event.h"
#include "process.h"

namespace Viso {

class Animation
{
private:
    Time m_time = 0;
    std::vector<Event*> m_events;
    std::vector<Process*> m_proc;
public:
    Animation();

    void add(Event *event);
    void add(Process *process);

    void seek(Time time);

    std::string getTimestamp() const;

    void update(Time delta);
};

} // ns Viso

#endif // VISO_ANIMATION_H_
