
#ifndef VISO_RAY_H_
#define VISO_RAY_H_

#include "common.h"

namespace Viso {

class Ray
{
public:
    Ray();
    ~Ray();

    glm::vec4 position;
    glm::vec4 direction;
};

} // ns Viso

#endif // VISO_RAY_H_
