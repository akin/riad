
#ifndef VISO_SCENE_H_
#define VISO_SCENE_H_

#include "../common.h"
#include "node.h"
#include <openvdb/openvdb.h>

namespace Viso {

class Scene
{
public:
    using Shared = std::shared_ptr<Scene>;
private:
    openvdb::FloatGrid::Ptr m_grid;

    std::vector<Node*> m_nodes;
public:
    Scene();
    ~Scene();

    void add(Node* node);
    void commit();
};

} // ns Viso

#endif // VISO_SCENE_H_
