
#ifndef VISO_NODE_H_
#define VISO_NODE_H_

#include "../common.h"

namespace Viso {

class Node
{
public:
    using Shared = std::shared_ptr<Node>;
private:
    Matrix m_transform;

    glm::vec3 m_position;
    glm::vec3 m_scale;
    glm::quat m_orientation;

    std::string m_name;

    bool m_dirty = true;
public:
    Node();
    virtual ~Node();

    void setPosition(glm::vec3 value);
    void setScale(glm::vec3 value);
    void setOrientation(glm::quat value);

    const std::string& getName() const;
    void setName(const std::string& value);

    void commit();

    const Matrix& getTransform() const;
};

} // ns Viso

#endif // VISO_NODE_H_
