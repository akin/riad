
#ifndef VISO_RENDERDOMAIN_H_
#define VISO_RENDERDOMAIN_H_

#include "common.h"

namespace Viso {

class RenderDomain
{
private:
    Float m_halfWidth;
    Float m_halfHeight;
    Float m_pixelStep;
    Float m_near;

    glm::mat4 m_screenToCamera;
    glm::mat4 m_cameraMatrix;
public:
    RenderDomain()
    {
    }

    ~RenderDomain()
    {
    }

    void setup(Float width, Float height, Float step, Float near, const glm::mat4& screenToCamera, const glm::mat4& position)
    {
        m_halfWidth = 0.5f * width;
        m_halfHeight = 0.5f * height;

        m_pixelStep = step;
        m_near = near;
        m_screenToCamera = screenToCamera;
        m_cameraMatrix = position;
    }

    void get(size_t x, size_t y, glm::vec4& position, glm::vec4& direction)
    {
        glm::vec4 pixelPos;
        glm::vec4 pixelDir;

        // Center the texture, x,y, to camera center.
        // the "film" is at near.. plane
        // TODO, pixelStep might be overstepping it. (remove it).
        // populate to Vec4
        pixelPos.x = (x - m_halfWidth) * m_pixelStep;
        pixelPos.y = (y - m_halfHeight) * m_pixelStep;
        pixelPos.z = m_near;
        pixelPos.w = 1.0;

        // well the direction is the same as position.. Z + forward.
        pixelDir = pixelPos;
        pixelDir.z += 10.0f;

        // Transform screenspace "ray" direction, by bending it with inverse projection
        pixelDir = m_screenToCamera * pixelDir;
        pixelDir.w = 1.0f;

        // travel the position to camera position
        position = m_cameraMatrix * pixelPos;
        position.w = 1.0;

        // travel the position to camera position, subtract position, and you get direction
        direction = (m_cameraMatrix * pixelDir) - position;
        direction.w = 0.0; // fix W to 0.0, as it is direction!

        // normalize direction
        direction = glm::normalize(direction);
    }
};

} // ns Viso

#endif // VISO_RENDERDOMAIN_H_
