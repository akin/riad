
#ifndef GFX_TRACING_H_
#define GFX_TRACING_H_

#include <string>
#include <base/common.h>
#include <viso/ray.h>
#include <viso/storage.h>
#include <viso/hit.h>

namespace Interface {

class Tracing
{
    BASE_ClassDef(Tracing);
public:
    virtual ~Tracing() = 0;

    // generator
    virtual void trace(const Viso::Ray& ray, Viso::Storage& storage) = 0;

    virtual void closestHit(const Viso::Ray& ray, Viso::Storage& storage, const Hit& hit) = 0;
    virtual void miss(const Viso::Ray& ray, Viso::Storage& storage) = 0;
    virtual void hit(const Viso::Ray& ray, Viso::Storage& storage, const Viso::Hit& hit) = 0;
    virtual void intersection(const Viso::Ray& ray, Viso::Storage& storage, const Viso::Intersection& intersection) = 0;
};

} // ns Interface

#endif // GFX_TRACING_H_