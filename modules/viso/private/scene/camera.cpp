
#include <viso/scene/camera.h>
#include <glm/gtc/matrix_transform.hpp> // matrix lookAt & projections

namespace Viso {

Camera::Camera()
{
}

Camera::~Camera()
{
}

void Camera::setLookAt(glm::vec3 pos)
{
}

void Camera::setNear(Float value)
{
    m_near = value;
}

void Camera::setFar(Float value)
{
    m_far = value;
}

Float Camera::getNear() const
{
    return m_near;
}

Float Camera::getFar() const
{
    return m_far;
}

void Camera::setupPerspective(Float hfov, Float width, Float height)
{
    m_projection = glm::perspective(
                hfov, 
                height / width, 
                m_near, 
                m_far);
}

void Camera::setupFrustum(Float left, Float right, Float top, Float bottom)
{
    m_projection = glm::frustum(
                left, 
                right,
                bottom,
                top,
                m_near, 
                m_far);
}

void Camera::setupOrtho(Float left, Float right, Float top, Float bottom)
{
    m_projection = glm::ortho(
                left, 
                right,
                bottom,
                top,
                m_near, 
                m_far);
}

const Matrix& Camera::getProjection() const
{
    return m_projection;
}

} // ns Viso
