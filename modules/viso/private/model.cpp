
#include <viso/model.h>


namespace Viso {

Model::Model()
{
}

Model::~Model()
{
}

Resource& Model::getResource()
{
    return m_resources;
}

Animation& Model::getAnimation()
{
    return m_animation;
}

Scene& Model::getScene()
{
    return m_scene;
}

Graphics::RGBf Model::cast(const Ray& ray)
{
    return Graphics::RGBf{
        ray.direction.x * -1,
        ray.direction.y * -1,
        ray.direction.z * -1
    };
}

void Model::update(Time delta)
{
    m_animation.update(delta);
}

void Model::commit()
{
    m_scene.commit();
}

} // ns Viso