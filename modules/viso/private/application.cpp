
#include <viso/application.h>
#include <viso/scene/camera.h>
#include <viso/scene/scene.h>
#include <viso/recorder.h>
#include <iostream>
#include <openvdb/openvdb.h>
#include <openvdb/tools/SignedFloodFill.h>
#include <openvdb/tools/RayIntersector.h>

#include <base/local.h>
#include <graphics/save.h>

#include <base/log.h>

#include <graphics/savetask.h>
#include "animation/testprocess.h"

namespace Viso {

Application::Application()
: Interface::Application("Raytracer")
, m_pool(m_manager)
{
}

Application::~Application()
{
}

bool Application::init()
{
    return true;
}

template<class GridType>
void makeSphere(GridType& grid, float radius, const openvdb::Vec3f& c)
{
    typedef typename GridType::ValueType ValueT;
    // Distance value for the constant region exterior to the narrow band
    const auto outside = grid.background();
    // Distance value for the constant region interior to the narrow band
    // (by convention, the signed distance is negative in the interior of
    // a level set)
    const auto inside = -outside;
    // Use the background value as the width in voxels of the narrow band.
    // (The narrow band is centered on the surface of the sphere, which
    // has distance 0.)
    int padding = int(openvdb::math::RoundUp(openvdb::math::Abs(outside)));
    // The bounding box of the narrow band is 2*dim voxels on a side.
    int dim = int(radius + padding);
    // Get a voxel accessor.
    auto accessor = grid.getAccessor();
    // Compute the signed distance from the surface of the sphere of each
    // voxel within the bounding box and insert the value into the grid
    // if it is smaller in magnitude than the background value.
    openvdb::Coord ijk;
    int &i = ijk[0], &j = ijk[1], &k = ijk[2];
    for (i = c[0] - dim; i < c[0] + dim; ++i) {
        const float x2 = openvdb::math::Pow2(i - c[0]);
        for (j = c[1] - dim; j < c[1] + dim; ++j) {
            const float x2y2 = openvdb::math::Pow2(j - c[1]) + x2;
            for (k = c[2] - dim; k < c[2] + dim; ++k) {
                // The distance from the sphere surface in voxels
                const float dist = openvdb::math::Sqrt(x2y2
                    + openvdb::math::Pow2(k - c[2])) - radius;
                // Convert the floating-point distance to the grid's value type.
                ValueT val = ValueT(dist);
                // Only insert distances that are smaller in magnitude than
                // the background value.
                if (val < inside || outside < val) continue;
                // Set the distance for voxel (i,j,k).
                accessor.setValue(ijk, val);
            }
        }
    }
    // Propagate the outside/inside sign information from the narrow band
    // throughout the grid.
    openvdb::tools::signedFloodFill(grid.tree());
}

template <typename GridType>
class Tracer
{
private:
    using Grid = GridType;
    using Intersector = openvdb::tools::LevelSetRayIntersector<Grid>;
    using Vec3 = typename Intersector::Vec3Type;
    using Ray = typename Intersector::RayType;
    using Value = typename Grid::ValueType;
    using Tree = typename Grid::TreeType;

    Intersector m_intersector;
public:
    Tracer(Grid& grid)
    : m_intersector(grid)
    {
    }

    bool trace(const Ray& ray)
    {
        // bool trace(size_t screen_x, size_t screen_y)
        Vec3 position;
        Vec3 normal;
        /*
        Film::RGBA& bg = mCamera->pixel(screen_x, screen_y);
        RayType ray = mCamera->getRay(screen_x, screen_y);//primary ray

        const BaseShader& shader = *mShader;
        Film::RGBA c = mInter.intersectsWS(ray, position, normal) ? shader(position, normal, ray.dir()) : bg;

        const float frac = 1.0f / (1.0f + mSubPixels);
        for (size_t k=0; k<mSubPixels; ++k, n +=2 ) 
        {
            ray = mCamera->getRay(screen_x, screen_y, mRand[n & 15], mRand[(n+1) & 15]);
            c += mInter.intersectsWS(ray, position, normal) ? shader(position, normal, ray.dir()) : bg;
        }//loop over sub-pixels
        bg = c*frac;
        */
        return m_intersector.intersectsWS(ray, position, normal);
    }
};

bool gridder(double value)
{
    value = glm::abs(value);
    auto v1 = glm::distance(value, 0.0);
    auto v2 = glm::distance(value, 1.0);
    return (v1 < 0.002 || v2 < 0.002);
}

int Application::run()
{
    LOG_MESSAGE("Hello {0}", "WORLD");

    // Initialize the OpenVDB library.  This must be called at least
    // once per program and may safely be called multiple times.
    openvdb::initialize();
    // Create an empty floating-point grid with background value 0.
    auto grid = openvdb::FloatGrid::create();

    makeSphere(*grid, 50.0, {1.5, 2, 3});

    Tracer<openvdb::FloatGrid> tracer(*grid);

    recorder.setup(1280, 720);

    auto camera = new Camera;
    
    int mode = 2;
    switch(mode)
    {
        case 0:
        {
            auto halfw = recorder.getWidth() * 0.5f;
            auto halfh = recorder.getHeight() * 0.5f;

            camera->setupOrtho(
                -halfw,
                halfw,
                halfh,
                -halfh);
            break;
        }
        case 1:
        {
            camera->setupPerspective(
                45.0f,
                recorder.getWidth(),
                recorder.getHeight()
            );
            break;
        }
        case 2:
        {
            auto halfw = 0.5f;
            auto halfh = 0.5f;

            camera->setupFrustum(
                -halfw,
                halfw,
                halfh,
                -halfh);
            break;
        }
    }
    
    camera->setPosition({
       0, 0, -10
    });
    camera->setName("test");
    
    model.getAnimation().add(new TestProcess{camera});
    model.getScene().add(camera);
    
    for(size_t i = 0 ; i < 1000 ; ++i)
    {
        model.commit();
        recorder.setTaskCount(m_pool.size());

        recorder.record(*camera, m_manager, [this](const Ray& ray, ColorModel& out){
            out = model.cast(ray);
        });
        
        // save image
        // ffmpeg -framerate 50 -pattern_type glob -i '*.png' -c:v libx264 out.mp4
        // ffmpeg -r 50 -pattern_type glob -i '*.png' -vb 20M myvideo.mpg
        {
            Graphics::Texture2D<ColorModel> hdr(recorder.getTexture());
            
            hdr.filter([](Graphics::RGBf& pixel){
                pixel = Graphics::RGBf{
                    color::get::red(pixel) * 0.5f + 0.5f,
                    color::get::blue(pixel) * 0.5f + 0.5f,
                    color::get::green(pixel) * 0.5f + 0.5f
                };
            });
            hdr.filter([](Graphics::RGBf& pixel){
                pixel = Graphics::RGBf{
                    std::min(color::get::red(pixel), 1.0f),
                    std::min(color::get::blue(pixel), 1.0f),
                    std::min(color::get::green(pixel), 1.0f)
                };
            });
            std::string path = camera->getName() + "." + model.getAnimation().getTimestamp() + ".png";

            auto gfxTask = new Graphics::SaveTask<Graphics::RGB8>();
            gfxTask->setFilename(path);
            gfxTask->getTexture().set(hdr);
            auto task = std::shared_ptr<Interface::Task>(gfxTask);

            m_manager.add(task);
        }
        model.update(0.01f);
    }

	return EXIT_SUCCESS;
}

} // ns Viso
