

#include <viso/animation/event.h>

namespace Viso {

Event::~Event()
{
}

Time Event::getTime() const 
{ 
    return m_time;
}

void Event::setTime(Time time) 
{ 
    m_time = time;
}

} // ns Viso

