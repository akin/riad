

#include <viso/animation/process.h>
#include <viso/scene/node.h>
#include <glm/gtc/quaternion.hpp> 
#include <glm/gtx/quaternion.hpp>
#include <base/log.h>

namespace Viso {

class TestProcess : public Process
{
private:
    Node *m_node = nullptr;
public:
    TestProcess(Node *node = nullptr)
    : m_node(node)
    {
    }

    virtual ~TestProcess()
    {
    }

    void set(Node *node)
    {
        m_node = node;
    }

    void update(Time time) override
    {
        assert(m_node != nullptr);

        float amount = time * 45.f;

        LOG_MESSAGE("Rotating {0} degrees, time {1}s", amount, time);

        glm::quat value = glm::angleAxis(glm::radians(amount), glm::vec3(0.f, 1.f, 0.f));
        m_node->setOrientation(value);
    }
};

} // ns Viso

