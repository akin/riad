

add_subdirectory("${PROJECT_MODULES}/base")
add_subdirectory("${PROJECT_MODULES}/base-tools")
add_subdirectory("${PROJECT_MODULES}/graphics-tools")
#add_subdirectory("${PROJECT_MODULES}/scripting-mono")
add_subdirectory("${PROJECT_MODULES}/viso")
add_subdirectory("${PROJECT_MODULES}/three")
add_subdirectory("${PROJECT_MODULES}/testbed")
add_subdirectory("${PROJECT_MODULES}/spirvjam")

if(NETWORK_LIB)
    add_subdirectory("${PROJECT_MODULES}/xhttp")
endif()

