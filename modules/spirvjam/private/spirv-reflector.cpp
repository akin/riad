#include <spirvjam/spirv-reflector.h>
#include <iostream>
#include <vulkan/spirv.hpp>
#include "inst.h"

// source material
// https://youtu.be/3Py4GlWAicY?t=6718 
// https://www.khronos.org/registry/spir-v/specs/1.0/SPIRV.pdf
namespace SpirvJam
{
	namespace
	{
		class Header
		{
		public:
			uint32_t magic;
			uint8_t version[4];
			uint32_t generator_version;
			uint32_t bound;
			uint32_t instruction_schema;
		};

		class Instruction
		{
		public:
			uint16_t code;
			uint16_t count;
		};
	}

	class Parser
	{
	private:
		std::vector<uint32_t> data;
		size_t iter = 0;
		size_t size = 0;

		Parsers m_parsers;

		Header *header;
	public:
		void resize(size_t size)
		{
			data.resize(size);
			this->size = size;
		}

		Parsers& getParsers() 
		{
			return m_parsers;
		}

		uint32_t *accessRaw()
		{
			return data.data();
		}

		void begin()
		{
			iter = 0;
		}

		template <class CType> 
		void get(CType*& target)
		{
			target = static_cast<CType*>((void*)(data.data() + iter));
		}

		bool parse()
		{
			begin();

			if (size < sizeof(Header))
			{
				return false;
			}

			get<Header>(header);
			if (header->magic != spv::MagicNumber)
			{
				return false;
			}
			iter += 5;

			Instruction* instruction;
			while (iter < size)
			{
				get<Instruction>(instruction);
				size_t content = iter + 1;

				auto func = m_parsers.find(instruction->code);
				if(func != m_parsers.end())
				{
					// -1 as we already consumed one word.
					func->second(instruction->count - 1, (data.data() + content), data.size() - content);
				}
				else
				{
					std::cout << "Unknown instruction " << instruction->code << std::endl;
				}

				iter += instruction->count;
			}
			return true;
		}
	};

	bool load(const std::string& path)
	{
		Parser parser;
		{
			FILE *file = fopen(path.c_str(), "rb");
			if (file == nullptr)
			{
				return false;
			}
			fseek(file, 0, SEEK_END);
			size_t fsize = ftell(file);
			fseek(file, 0, SEEK_SET);

			size_t total = fsize / sizeof(uint32_t);
			parser.resize(total);

			fread(parser.accessRaw(), sizeof(uint32_t), total, file);
			fclose(file);
		}

		loadCommon(parser.getParsers());

		// https://en.wikipedia.org/wiki/Recursive_descent_parser
		return parser.parse();
	}
} // ns SpirvJam
