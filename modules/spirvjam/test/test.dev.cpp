
#ifndef SPRIVJAM_DEV_TEST_H_
#define SPRIVJAM_DEV_TEST_H_

#include <iostream>
#include "catch.hpp"

#include <spirvjam/spirv-reflector.h>

TEST_CASE("SimpleStart spirv-reflector.", "[]")
{
	REQUIRE(SpirvJam::load("test.frag.spv"));

	system("pause");
}


#endif // SPRIVJAM_DEV_TEST_H_
