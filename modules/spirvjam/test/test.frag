#version 450
layout(binding = 0) uniform sampler2D tex;
layout(location = 3) in vec3 vpos;
layout(location = 4) in vec3 norm;
layout(location = 5) in vec3 ldir;
layout(location = 6) in vec2 texcoord;
layout(location = 0) out vec4 color;
void main()
{
    vec4 texel = texture(tex, texcoord);
    vec3 vdir = -normalize(vpos);
    vec3 n = normalize(norm);
    vec3 l = normalize(ldir);
    vec3 h = normalize(vdir + ldir);
    float ndotl = max(dot(n, l), 0.0);
    float ndoth = max(dot(n, h), 0.0);
    vec3 diffuse = texel.rgb * ndotl;
    vec3 specular = vec3(1.0, 1.0, 1.0) * pow(ndoth, 50.0);
    color.rgb = diffuse + specular;
    color.a = texel.a;
}