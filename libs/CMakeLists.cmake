
add_subdirectory("${PROJECT_LIBS}/catch")
add_subdirectory("${PROJECT_LIBS}/stb")

set(GLM_TEST_ENABLE OFF)
set(GLM_INSTALL_ENABLE OFF)
add_definitions(-DGLM_ENABLE_EXPERIMENTAL)
add_subdirectory("${PROJECT_LIBS}/glm")

set(FMT_TEST OFF)
set(FMT_INSTALL OFF)
set(FMT_DOC OFF)
add_subdirectory("${PROJECT_LIBS}/color")
add_subdirectory("${PROJECT_LIBS}/fmt")
add_subdirectory("${PROJECT_LIBS}/json")
add_subdirectory("${PROJECT_LIBS}/sqlite3")
#add_subdirectory("${PROJECT_LIBS}/openvdb/openvdb")
